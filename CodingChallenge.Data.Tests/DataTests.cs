﻿using System;
using System.Collections.Generic;
using CodingChallenge.Data.Classes;
using NUnit.Framework;
using CodingChallenge.Data.Constants;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>", PrinterFormasGeometricas.Imprimir(new List<FormaGeometrica>(), LanguagesCodes.SpanishCode));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>", PrinterFormasGeometricas.Imprimir(new List<FormaGeometrica>(), LanguagesCodes.EnglishCode));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnFrances()
        {
            Assert.AreEqual("<h1>Liste de formes vide!</h1>", PrinterFormasGeometricas.Imprimir(new List<FormaGeometrica>(), LanguagesCodes.FrenchCode));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            //Arrange
            var cuadrados = new List<FormaGeometrica> { new Cuadrado(5) };  //cuadrado de lado 5

            //Act
            string resumen = PrinterFormasGeometricas.Imprimir(cuadrados, LanguagesCodes.SpanishCode);

            //Assert
            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            //Arrange
            var cuadrados = new List<FormaGeometrica>
            {
                new Cuadrado(5),    //cuadrado lado 5
                new Cuadrado(1),    //cuadrado lado 1
                new Cuadrado(3),    //cuadrado lado 3
            };

            //Act
            var resumen = PrinterFormasGeometricas.Imprimir(cuadrados, LanguagesCodes.EnglishCode);

            //Assert
            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            //Arrange
            var formas = this.getFormasVarias();

            //Act
            var resumen = PrinterFormasGeometricas.Imprimir(formas, LanguagesCodes.EnglishCode);

            //Assert
            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            //Arrange
            var formas = getFormasVarias();

            //Act
            var resumen = PrinterFormasGeometricas.Imprimir(formas, LanguagesCodes.SpanishCode);

            //Assert
            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perimetro 28 <br/>2 Círculos | Area 13,01 | Perimetro 18,06 <br/>3 Triángulos | Area 49,64 | Perimetro 51,6 <br/>TOTAL:<br/>7 formas Perimetro 97,66 Area 91,65",
                resumen);
        }

        // Tests agregados
        [TestCase]
        public void TestArgumentExceptionWhenCreateFormaGeometricaInvalid()
        {
            Assert.Throws<ArgumentException>(() => new Cuadrado(-5));
        }

        [TestCase]
        public void TestLenguajeNoPermitido()
        {
            Assert.Throws<ArgumentException>(() => PrinterFormasGeometricas.Imprimir(new List<FormaGeometrica>(), "BR"));
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnFrances()
        {
            //Arrange
            var formas = this.getFormasVarias();

            //Act
            var resumen = PrinterFormasGeometricas.Imprimir(formas, LanguagesCodes.FrenchCode);

            //Assert
            Assert.AreEqual(
                "<h1>Rapport sur les formes</h1>2 Carrés | Zone 29 | Périmètre 28 <br/>2 Cercles | Zone 13,01 | Périmètre 18,06 <br/>3 Triangles | Zone 49,64 | Périmètre 51,6 <br/>LE TOTAL:<br/>7 formes Périmètre 97,66 Zone 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConUnRectangulo()
        {
            //Arrange
            var rectangulo = new List<FormaGeometrica> { new Rectangulo(3,4) };  //rectangulo de lados 3 y 4

            //Act
            string resumen = PrinterFormasGeometricas.Imprimir(rectangulo, LanguagesCodes.SpanishCode);

            //Assert
            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Rectangulo | Area 12 | Perimetro 14 <br/>TOTAL:<br/>1 formas Perimetro 14 Area 12", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnTrapecioIsoscelesEnIngles()
        {
            //Arrange
            var trapecio = new List<FormaGeometrica> { new TrapecioIsosceles(12,17,8) };  //trapecio b=12, B=17, h=8

            //Act
            string resumen = PrinterFormasGeometricas.Imprimir(trapecio, LanguagesCodes.EnglishCode);

            //Assert
            Assert.AreEqual("<h1>Shapes report</h1>1 Trapezium | Area 116 | Perimeter 45,76 <br/>TOTAL:<br/>1 shapes Perimeter 45,76 Area 116", resumen);
        }

        [TestCase]
        public void TestResumenListaConRectanguloYTrapecio()
        {
            //Arrange
            var formas = new List<FormaGeometrica> { new Rectangulo(3,4), new TrapecioIsosceles(12, 17, 8) };  //rectangulo l1=3, l2=4 y trapecio b=12, B=17, h=8

            //Act
            string resumen = PrinterFormasGeometricas.Imprimir(formas, LanguagesCodes.SpanishCode);

            //Assert
            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Rectangulo | Area 12 | Perimetro 14 <br/>1 Trapecio | Area 116 | Perimetro 45,76 <br/>TOTAL:<br/>2 formas Perimetro 59,76 Area 128", resumen);
        }

        public List<FormaGeometrica> getFormasVarias()
        {
            var listFormas = new List<FormaGeometrica>
            {
                new Cuadrado(5),                //Cuadrado de lado 5
                new Circulo(1.5m),              //Circulo de radio 3/2 = 1.5m
                new TrianguloEquilatero(4),     //Triangulo equilatero de lado 4
                new Cuadrado(2),                //Cuadrado de lado 2
                new TrianguloEquilatero(9),     //Cuadrado de lado 9
                new Circulo(2.75m / 2),         //Circulo de radio 2.75m/2 = 1.375m
                new TrianguloEquilatero(4.2m),  //Triangulo equilatero de radio 4.2m
            };

            return listFormas;
        }
    }
}
