
# InvertirOnline.com Coding Challenge

## Resolución del problema

Se realizó un refactoring del código para mejorar la escalabilidad y mantenibilidad del mismo. Permitiendo de esta forma, agregar nuevas figuras geométricas o mostrar el informe en diferentes idiomas.  

* Se agregaron 2 figuras más: **Rectángulo** y **Trapecio Isósceles**
* Se agregó un nuevo idioma: Frances (**FR**)
* Se agregaron nuevos **test** para probar las nuevas funcionalidades, el refactoring y algunas condiciones de error.

### Refactoring general

Se modificó la estructura del proyecto para reflejar el refactoring de clases y recursos y separar responsabilidades. Ya que en el desafío inicial se encontraba todo en una sóla clase.

* Se agregó la carpeta **Formas** que contiene las clases con cada tipo de forma.  
* Se agregó una clase con constantes de **códigos de Lenguajes** basándonos en la ISO-639-1.
* Se agregó una carpeta con los **recursos** de los textos en cada lenguaje utilizado.

Nueva estructura:  
![image.png](./image.png)

### Refactoring de Formas

Para el refactoring de formas se creó una clase abstracta **FormaGeometrica** con sus métodos abstractos CalcularArea y CalcularPerimetro, para que cada subclase que haya de forma geométrica herede esto y lo implemente.  
Se crearon las clases de los distintos tipos de formas que heredan de FormaGeometrica (Circulo, Cuadrado, Rectangulo, etc.) cada uno definiendo su comportamiento.  

Se separó la clase de impresión de formas **PrinterFormasGeometricas** que contiene la lógica de impresión y que hace uso de las formas que se le pasen al printer. Además tiene en cuenta el idioma con el cuál se quiere imprimir el reporte.

### Refactoring de Lenguajes / Idiomas

Para el refactoring de lenguajes/idiomas, lo que se hizo fue crear recursos **.resx** con cada lenguaje habilitado, y seteando las variables que necesitamos utilizar en el reporte.  
Se creó una clase con constantes de los códigos de dos caracteres definidos en la ISO-639-1 para mapear con los recursos.

### Tests

* Se agregaron Tests para chequear el nuevo idioma agregado (Francés - FR)
* Se agregaron test para chequear Excepciones si la figura geométrica es inválida o el lenguaje ingresado no está permitido.
* Se agregaron tests para validar las nuevas figuras creadas (Rectángulo y Trapecio Isósceles)



