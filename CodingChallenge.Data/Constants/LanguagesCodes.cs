﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Constants
{
    public class LanguagesCodes
    {
        //Langages Codes ISO-639-1
        public static readonly string SpanishCode = "ES";
        public static readonly string EnglishCode = "EN";
        public static readonly string FrenchCode = "FR";
    }
}
