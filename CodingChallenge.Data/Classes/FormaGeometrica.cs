﻿using System;

namespace CodingChallenge.Data.Classes
{
    public abstract class FormaGeometrica
    {
        //attributes
        protected decimal lado1 { get; set; }

        //constructor
        public FormaGeometrica(decimal _lado1)
        {
            if(_lado1 > 0)
            {
                this.lado1 = _lado1;
            }
            else
            {
                throw new ArgumentException(String.Format("{0} no es un lado válido", lado1),"lado1");
            }
        }

        public FormaGeometrica() {}

        //abstract methods
        public abstract decimal CalcularArea();
        public abstract decimal CalcularPerimetro();
    }
}
