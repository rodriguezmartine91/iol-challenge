﻿/*
 * Refactorear la clase para respetar principios de programación orientada a objetos. Qué pasa si debemos soportar un nuevo idioma para los reportes, o
 * agregar más formas geométricas?
 *
 * Se puede hacer cualquier cambio que se crea necesario tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Trapecio/Rectangulo, agregar otro idioma a reporting.
 * */

using CodingChallenge.Data.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;

namespace CodingChallenge.Data.Classes
{
    public class PrinterFormasGeometricas
    {
        public List<FormaGeometrica> formas = new List<FormaGeometrica>();

        // array con referencias de los nombres de las figuras
        public static string[] referenciasFiguras = new string[] { nameof(Cuadrado), nameof(Circulo), nameof(TrianguloEquilatero), nameof(Rectangulo), nameof(TrapecioIsosceles) };

        // diccionary to map the languageCode with the local ResourceManager
        public static Dictionary<string, ResourceManager> mapCodeLangResource = new Dictionary<string, ResourceManager>
        {
            { Constants.LanguagesCodes.SpanishCode, ResourceES.ResourceManager },
            { Constants.LanguagesCodes.EnglishCode, ResourceEN.ResourceManager },
            { Constants.LanguagesCodes.FrenchCode, ResourceFR.ResourceManager }
        };

        public PrinterFormasGeometricas(List<FormaGeometrica> _formas)
        {
            this.formas = _formas;
        }

        public PrinterFormasGeometricas()
        {

        }

        public void addNewFormaGeometrica(FormaGeometrica forma)
        {
            this.formas.Add(forma);
        }

        public static string Imprimir(List<FormaGeometrica> formas, string LangCode)
        {
            var sb = new StringBuilder();

            if (!formas.Any())  // No hay formas cargadas
            {
                sb.Append(getEmptyListText(LangCode));
            }
            else    // Hay por lo menos una forma
            {
                // HEADER
                sb.Append(getHeaderText(LangCode));

                // arrays emparejados con referenciasFiguras, con uno llevo las referencias y con los otros las cantidades y sumas.
                // cantidad de figuras de cada tipo
                int[] cantFiguras = new int[referenciasFiguras.Length];
                // suma de áreas de cada tipo de figura
                decimal[] sumAreas = new decimal[referenciasFiguras.Length];
                // suma de perímetros de cada tipo de figura
                decimal[] sumPerimetros = new decimal[referenciasFiguras.Length];

                foreach(FormaGeometrica forma in formas)
                {
                    int indexRefArray = Array.IndexOf(referenciasFiguras, forma.GetType().Name);

                    cantFiguras[indexRefArray] += 1;
                    sumAreas[indexRefArray] += forma.CalcularArea();    // área de la figura en particular
                    sumPerimetros[indexRefArray] += forma.CalcularPerimetro();  // perímetro de la figura en particular
                }

                // recorremos los arrays para agregar los outputs de las figuras que tengan cant > 0
                for(int i = 0; i < referenciasFiguras.Length; i++)
                {
                    if (cantFiguras[i] > 0) // hay al menos una figura de el tipo del index i
                    {
                        sb.Append(ObtenerLinea(cantFiguras[i], sumAreas[i], sumPerimetros[i], referenciasFiguras[i], LangCode));
                    }
                }

                // FOOTER
                sb.Append(getFooterText(cantFiguras, sumAreas, sumPerimetros, LangCode));
            }

            return sb.ToString();
        }

        private static string getEmptyListText(string lengCode)
        {
            ResourceManager rm = getResourceManagerFromLangCode(lengCode);
            return "<h1>" + rm.GetString("StrEmptyList") + "</h1>";
        }

        private static string getHeaderText(string lengCode)
        {
            ResourceManager rm = getResourceManagerFromLangCode(lengCode);
            return "<h1>" + rm.GetString("StrHeaderText") + "</h1>";
        }

        private static string getFooterText(int[] cantFiguras, decimal[] areas, decimal[] perimetros, string lengCode)
        {
            StringBuilder res =  new StringBuilder();
            int totalFormas = cantFiguras.Sum();
            decimal totalAreas = areas.Sum();
            decimal totalPerimetros = perimetros.Sum();

            ResourceManager rm = getResourceManagerFromLangCode(lengCode);
            res.Append(rm.GetString("StrTotal") +":<br/>");
            res.Append(totalFormas + " " + rm.GetString("StrFormas") + " ");
            res.Append(rm.GetString("StrPerimetro") + " " + (totalPerimetros).ToString("#.##") + " ");
            res.Append(rm.GetString("StrArea") + " " + (totalAreas).ToString("#.##"));

            return res.ToString();
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, string tipo, string lengCode)
        {
            ResourceManager rm = getResourceManagerFromLangCode(lengCode);
            if (cantidad > 0)
                return $"{cantidad} {TraducirForma(tipo, cantidad, lengCode)} | {rm.GetString("StrArea")} {area:#.##} | {rm.GetString("StrPerimetro")} {perimetro:#.##} <br/>";
            else
                return string.Empty;
        }


        private static string TraducirForma(string tipo, int cantidad, string lengCode)
        {
            ResourceManager rm = getResourceManagerFromLangCode(lengCode);
            return cantidad == 1 ? rm.GetString("Str" + tipo) : rm.GetString("Str" + tipo) + "s";
        }

        private static ResourceManager getResourceManagerFromLangCode(string LangCode)
        {
            ResourceManager rm;
            if(mapCodeLangResource.TryGetValue(LangCode, out rm)){
                return rm;
            }
            else
            {
                throw new ArgumentException(LangCode + " no es un lenguaje permitido");
            }
        } 

    }
}
