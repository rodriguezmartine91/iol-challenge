﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Classes
{
    public class TrapecioIsosceles : FormaGeometrica
    {
        private decimal baseMenor { get; set; }
        private decimal baseMayor { get; set; }
        private decimal altura { get; set; }
        public TrapecioIsosceles(decimal _baseMenor, decimal _baseMayor, decimal _altura)
        {
            if (_baseMenor > 0 && _baseMayor > 0 && _altura > 0 && _baseMenor < _baseMayor)
            {
                this.baseMenor = _baseMenor;
                this.baseMayor = _baseMayor;
                this.altura = _altura;
            }
            else
            {
                throw new ArgumentException("Los lados deben ser > 0 y la baseMenor < a la baseMayor");
            }
        }

        // área del trapecio = (B + b)/2 * h
        public override decimal CalcularArea()
        {
            return (baseMenor + baseMayor) / 2 * altura;
        }

        // perímetro del trapecio = b + B + 2*l
        // l = lado del trapecio = sqrt( ((B-b)/2)^2 + h^2 ) 
        public override decimal CalcularPerimetro()
        {
            // calculamos el lado del trapecio
            decimal aux = (baseMayor - baseMenor) / 2;
            decimal lado = (decimal)Math.Sqrt((double)(aux * aux  + altura * altura));
            return baseMenor + baseMayor + 2 * lado;
        }

    }
}
