﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Classes
{
    public class Rectangulo : FormaGeometrica
    {
        private decimal lado2 { get; set; }
        public Rectangulo(decimal _lado1, decimal _lado2) : base(_lado1)
        {
            if(_lado1 > 0 && _lado2 > 0){
                this.lado1 = _lado1;
                this.lado2 = _lado2;
            }
            else
            {
                throw new ArgumentException("Los lados deben ser > 0");
            }
        }

        public override decimal CalcularArea()
        {
            return lado1 * lado2;
        }

        public override decimal CalcularPerimetro()
        {
            return 2 * lado1 + 2 * lado2;
        }

    }
}
