﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Classes
{
    public class Circulo : FormaGeometrica
    {
        //in this case lado1 is the circle's radio
        public Circulo(decimal _radio) : base(_radio)
        {
        }

        public override decimal CalcularArea()
        {
            return (decimal)Math.PI * (lado1 * lado1);
        }

        public override decimal CalcularPerimetro()
        {
            return 2 * (decimal)Math.PI * lado1;
        }

    }
}
