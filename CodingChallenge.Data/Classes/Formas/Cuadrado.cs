﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Classes
{
    public class Cuadrado : FormaGeometrica
    {
        public Cuadrado(decimal _lado1) : base(_lado1)
        {
        }

        public override decimal CalcularArea()
        {
            return lado1 * lado1;
        }

        public override decimal CalcularPerimetro()
        {
            return lado1 * 4;
        }

    }
}
